require 'byebug'

class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    @grid.inject(0) { |count, pos| count + pos.count(:s) }
  end

  def empty?(pos = [0, 0])
    if self[pos] == nil
      return true
    elsif self[pos] == :s
      return false
    end
  end

  def full?
    @grid.each do |row|
      return false if row.include?(nil)
    end
  end

  def place_random_ship
    pos = @grid.each_with_index.inject([]) do |position, (row, i)|
      row.each_index do |j|
        position << [i, j] if row[j] == nil
      end

      position
    end

    if full?
      raise "There was an error"
    else
      x, y = pos.shuffle[-1]
      @grid[x][y] = :s
    end
  end

  def won?
    self.grid.none? { |row| row.include?(:s) }
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos)
    x, y = pos
    @grid[x][y]
  end
end
