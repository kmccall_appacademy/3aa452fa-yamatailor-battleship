class HumanPlayer

  attr_accessor :name

  def initialize(name = "Bob")
    @name = name
  end

  def get_play
    gets.chomp.split(',').map(&:to_i)
  end
end
